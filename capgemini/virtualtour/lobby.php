<?php
require_once "logincheck.php";
$curr_room = 'lobby';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="pano-view">
            <div id="controls">
                <div class="ctrl" id="pan-up">&#9650;</div>
                <div class="ctrl" id="pan-down">&#9660;</div>
                <div class="ctrl" id="pan-left">&#9664;</div>
                <div class="ctrl" id="pan-right">&#9654;</div>
                <div class="ctrl" id="zoom-in">&plus;</div>
                <div class="ctrl" id="zoom-out">&minus;</div>
            </div>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "wt.php" ?>
<?php require_once "scripts.php" ?>
<script>
    viewer = pannellum.viewer('pano-view', {
        "type": "equirectangular",
        "panorama": "assets/img/lobby.png",
        "autoLoad": true,
        "pitch": 0,
        "yaw": 0,
        "hfov": 100,
        "showControls": false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 5000,
        //"hotSpotDebug": true,
        "hotSpots": [{
                "pitch": -4.7,
                "yaw": -33,
                "cssClass": "dot",
                "clickHandlerFunc": gotoAgenda,

            },
            {
                "pitch": 8.4,
                "yaw": 170.49,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": 'https://player.vimeo.com/video/539038569',

            },
            {
                "pitch": 4.03,
                "yaw": -76.08,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": 'https://player.vimeo.com/video/539038512',

            },

        ]
    });

    function gotoAgenda() {
        location.href = 'agenda.php';
    }
</script>
<?php require_once "controls.php"; ?>

<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>