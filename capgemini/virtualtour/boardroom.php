<?php
require_once "logincheck.php";
$curr_room = 'boardroom';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="pano-view">
            <div id="controls">
                <div class="ctrl" id="pan-up">&#9650;</div>
                <div class="ctrl" id="pan-down">&#9660;</div>
                <div class="ctrl" id="pan-left">&#9664;</div>
                <div class="ctrl" id="pan-right">&#9654;</div>
                <div class="ctrl" id="zoom-in">&plus;</div>
                <div class="ctrl" id="zoom-out">&minus;</div>
            </div>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "wt.php" ?>
<?php require_once "scripts.php" ?>
<script>
    viewer = pannellum.viewer('pano-view', {
        "type": "equirectangular",
        "panorama": "assets/img/boardroom.jpg",
        "autoLoad": true,
        "pitch": 0,
        "yaw": 12,
        "hfov": 100,
        "showControls": false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 5000,
        //"hotSpotDebug": true,
        "hotSpots": [
            /* {
                "pitch": 1.31,
                "yaw": 90.56,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "kolkata.php",

            }, */
            {
                "pitch": 3.09,
                "yaw": -116.94,
                "cssClass": "dot",
                "clickHandlerFunc": goto360deg,
                /* "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/532097988", */
            },
            {
                "pitch": 2.52,
                "yaw": 149.35,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/565572378",
            },
            /* {
                "pitch": 3.06,
                "yaw": -42.87,
                "cssClass": "dot",
                "clickHandlerFunc": goto360deg,
            }, */
            {
                "pitch": 3.06,
                "yaw": -42.87,
                "cssClass": "dot",
                /* "clickHandlerFunc": showImage,
                "clickHandlerArgs": "assets/img/boardroom_tv.png", */
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/568810805",
            },
            {
                /* "pitch": 7.45,
                "yaw": 90.43, */
                "pitch": 1.31,
                "yaw": 90.56,
                "cssClass": "dot",
                "clickHandlerFunc": showImage,
                "clickHandlerArgs": "assets/img/success_story.jpg",
            },
            {
                "pitch": 4.26,
                "yaw": 16.54,
                "cssClass": "dot",
                "clickHandlerFunc": showImage,
                "clickHandlerArgs": "assets/img/testimonial_01.jpg",
            },
            {
                "pitch": 4.69,
                "yaw": 37.17,
                "cssClass": "dot",
                "clickHandlerFunc": showImage,
                "clickHandlerArgs": "assets/img/testimonial_02.jpg",
            },
            {
                "pitch": 3.43,
                "yaw": -67.31,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/565584478",
            },

        ]
    });

    function goto360deg() {
        window.open('https://indiavirtualtour.bsv.capgemini.com/');
    }
</script>
<?php require_once "controls.php"; ?>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>