<?php
require_once "logincheck.php";
$curr_room = 'engagement';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">

        <div id="pano-view">
            <div id="controls">
                <div class="ctrl" id="pan-up">&#9650;</div>
                <div class="ctrl" id="pan-down">&#9660;</div>
                <div class="ctrl" id="pan-left">&#9664;</div>
                <div class="ctrl" id="pan-right">&#9654;</div>
                <div class="ctrl" id="zoom-in">&plus;</div>
                <div class="ctrl" id="zoom-out">&minus;</div>
            </div>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "wt.php" ?>
<?php require_once "scripts.php" ?>
<script>
    viewer = pannellum.viewer('pano-view', {
        "type": "equirectangular",
        "panorama": "assets/img/engagement.png",
        "autoLoad": true,
        "pitch": 0,
        "yaw": 8,
        "hfov": 100,
        "showControls": false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 5000,
        //"hotSpotDebug": true,
        "hotSpots": [{
                "pitch": -6.2,
                "yaw": -14.3,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/568807113",
            },
            {
                "pitch": -0.63,
                "yaw": 89.88,
                "cssClass": "dot",
                "clickHandlerFunc": showImage,
                "clickHandlerArgs": "assets/img/journey_wall.png",
            },
            {
                "pitch": -2.9,
                "yaw": 140.9,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "syngenta.php",
            },
        ]
    });
</script>
<?php require_once "controls.php"; ?>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>