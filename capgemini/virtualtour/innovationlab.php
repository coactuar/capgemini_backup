<?php
require_once "logincheck.php";
$curr_room = 'lab';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="pano-view">
            <div id="controls">
                <div class="ctrl" id="pan-up">&#9650;</div>
                <div class="ctrl" id="pan-down">&#9660;</div>
                <div class="ctrl" id="pan-left">&#9664;</div>
                <div class="ctrl" id="pan-right">&#9654;</div>
                <div class="ctrl" id="zoom-in">&plus;</div>
                <div class="ctrl" id="zoom-out">&minus;</div>
            </div>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "wt.php" ?>
<?php require_once "scripts.php" ?>
<script>
    viewer = pannellum.viewer('pano-view', {
        "type": "equirectangular",
        "panorama": "assets/img/innovationlab.jpg",
        "autoLoad": true,
        "pitch": 0.5,
        "yaw": 20.9,
        "hfov": 100,
        "showControls": false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 5000,
        //"hotSpotDebug": true,
        "hotSpots": [
            /* {
                "pitch": -3.04,
                "yaw": -88.55,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "icc.php",
            },
            {
                "pitch": 1.53,
                "yaw": 136.6,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/539038243",
            },
            {
                "pitch": 0.93,
                "yaw": 44.66,
                "cssClass": "dot",
                "type": "info",
                "URL": "https://capgemini.sharepoint.com/sites/intelligent_commandcenter"
            }, */
            {
                "pitch": 2.03,
                "yaw": -0.34,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/565166923",
            },
            {
                "pitch": 1.05,
                "yaw": 89.67,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/565166840",
            },
            {
                "pitch": 2.28,
                "yaw": 179.4,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/565166951",
            },
            {
                "pitch": -0.25,
                "yaw": -131,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/565166860",
            },
            {
                "pitch": -0.16,
                "yaw": -47.6,
                "cssClass": "dot",
                "clickHandlerFunc": showContent,
                "clickHandlerArgs": "https://player.vimeo.com/video/565166882",
            },

        ]
    });
</script>
<?php require_once "controls.php"; ?>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>