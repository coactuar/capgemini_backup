<?php
require_once "../functions.php";
$title = 'Capgemini';

$fb = new Sentimeter();
$list = $fb->getAllFeedbacks();

$i = 0;
$data = array();
$ev = new Event();

foreach ($list as $c) {
  $data[$i]['Name'] = $c['name'];
  $data[$i]['Email ID'] = $c['emailid'];
  $data[$i]['Comment'] = $c['comment'];
  $data[$i]['Event Rating'] = $c['points'];
  $data[$i]['Feedback Time'] = $c['submitted_at'];

  $i++;
}
$filename = $title . "_feedbacks.xls";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"$filename\"");
ExportFile($data);
