<?php
require_once "../functions.php";
define('SITE_ROOT', realpath(dirname(__FILE__)));

$allowedFileType = ['application/vnd.ms-excel', 'text/xls', 'text/xlsx', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

$message = 'Error updating users';
if (in_array($_FILES["file"]["type"], $allowedFileType)) {

  $targetPath = 'uploads/' . $_FILES['file']['name'];
  //echo $targetPath;
  $status = move_uploaded_file($_FILES['file']['tmp_name'],  SITE_ROOT . '/' . $targetPath);

  $file = $targetPath;
  $handle = fopen($file, "r");
  $c = 0;
  $n = 0;
  $d = 0;
  $dup = '';
  while (($filesop = fgetcsv($handle, 1000, ",")) !== false) {
    $name = $filesop[0];
    $emailid = $filesop[1];

    if (($name == '') || ($emailid == '')) {
      //do nothing
      $message = "CSV File is missing data.";
    } else {
      $user = new User();
      $user->__set('name', $name);
      $user->__set('emailid', $emailid);
      $add = $user->addUser();
      //var_dump($add);
      $reg_status = $add['status'];
      if ($reg_status == "error") {
        $d = $d + 1;
        $dup .= $emailid . '<br>';
      }
      if ($reg_status == "success") {
        $n = $n + 1;
      }
    }

    $c = $c + 1;
  }
  $message = $n . " users imported in database. <br>";
  if ($d > 0) {
    $message .= $d . " duplicate records found.<br><br>";
    $message .= $dup;
  }
} else {
  $message = "Invalid File Type. Upload CSV File.";
}


echo $message;
