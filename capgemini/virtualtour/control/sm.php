<?php
require_once "../functions.php";

if (isset($_POST['action']) && !empty($_POST['action'])) {

  $action = $_POST['action'];

  switch ($action) {
    case 'getallfeedbacks':

      $page_no = $_POST['pagenum'];
      $offset = ($page_no - 1) * $total_records_per_page;
      $previous_page = $page_no - 1;
      $next_page = $page_no + 1;

      $feedback = new Sentimeter();
      $feedback->__set('limit', $total_records_per_page);
      $total_records = $feedback->getFBCount();

      $total_no_of_pages = ceil($total_records / $total_records_per_page);
      $second_last = $total_no_of_pages - 1; // total page minus 1

      $fbList = $feedback->getFeedbacks($offset);
      //var_dump($fbList);
      if (!empty($fbList)) {
?>
        <table class="table table-borderless">
          <tbody>
            <tr>
              <td scope="col"><b>Total Feedbacks:</b> <?= $total_records ?></td>
              <td scope="col"><a href="fb.php">Download Feedbacks</a></td>
            </tr>
            </ </table>
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col" width="150">Name</th>
                  <th scope="col" width="250">Email ID</th>
                  <th scope="col">Comment</th>
                  <th scope="col" width="150">Event Rating</th>
                  <th scope="col" width="150">Feedback Time</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($fbList as $a) { ?>
                  <tr>
                    <td><?= $a['name'] ?></td>
                    <td><?= $a['emailid'] ?></td>
                    <td><?= $a['comment'] ?></td>
                    <td><?= $a['points'] ?></td>
                    <td><?php
                        if ($a['submitted_at'] != NULL) {
                          $date = date_create($a['submitted_at']);
                          echo date_format($date, "M d, H:i a");
                        }
                        ?></td>

                  </tr>
                <?php } ?>
              </tbody>

            </table>

            <ul class="pagination">
              <?php // if($page_no > 1){ echo "<li><a href='?page_no=1'>First Page</a></li>"; } 
              ?>

              <li class='page-item <?php if ($page_no <= 1) {
                                      echo "disabled";
                                    } ?>'>
                <a class='page-link' <?php if ($page_no > 1) {
                                        echo "onClick='gotoPage($previous_page)' href='#'";
                                      } ?>>Previous</a>
              </li>

              <?php
              if ($total_no_of_pages <= 10) {
                for ($counter = 1; $counter <= $total_no_of_pages; $counter++) {
                  if ($counter == $page_no) {
                    echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                  } else {
                    echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                  }
                }
              } elseif ($total_no_of_pages > 10) {

                if ($page_no <= 4) {
                  for ($counter = 1; $counter < 8; $counter++) {
                    if ($counter == $page_no) {
                      echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                    } else {
                      echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                    }
                  }
                  echo "<li class='page-item'><a>...</a></li>";
                  echo "<li class='page-item'><a class='page-link' onClick='gotoPage($second_last)' href='#'>$second_last</a></li>";
                  echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>$total_no_of_pages</a></li>";
                } elseif ($page_no > 4 && $page_no < $total_no_of_pages - 4) {
                  echo "<li class='page-item'><a class='page-link' onClick='gotoPage(1)' href='#'>1</a></li>";
                  echo "<li class='page-item'><a class='page-link' onClick='gotoPage(2)' href='#'>2</a></li>";
                  echo "<li class='page-item'><a>...</a></li>";
                  for ($counter = $page_no - $adjacents; $counter <= $page_no + $adjacents; $counter++) {
                    if ($counter == $page_no) {
                      echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                    } else {
                      echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                    }
                  }
                  echo "<li class='page-item'><a>...</a></li>";
                  echo "<li class='page-item'><a class='page-link' onClick='gotoPage($second_last)' href='#'>$second_last</a></li>";
                  echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>$total_no_of_pages</a></li>";
                } else {
                  echo "<li class='page-item'><a class='page-link' onClick='gotoPage(1)' href='#'>1</a></li>";
                  echo "<li class='page-item'><a class='page-link' onClick='gotoPage(2)' href='#'>2</a></li>";
                  echo "<li class='page-item'><a>...</a></li>";

                  for ($counter = $total_no_of_pages - 6; $counter <= $total_no_of_pages; $counter++) {
                    if ($counter == $page_no) {
                      echo "<li class='page-item active'><a class='page-link'>$counter</a></li>";
                    } else {
                      echo "<li class='page-item'><a class='page-link' onClick='gotoPage($counter)' href='#'>$counter</a></li>";
                    }
                  }
                }
              }
              ?>

              <li class='page-item <?php if ($page_no >= $total_no_of_pages) {
                                      echo "disabled";
                                    } ?>'>
                <a class='page-link' <?php if ($page_no < $total_no_of_pages) {
                                        echo "onClick='gotoPage($next_page)' href='#'";
                                      } ?>>Next</a>
              </li>
              <?php if ($page_no < $total_no_of_pages) {
                echo "<li class='page-item'><a class='page-link' onClick='gotoPage($total_no_of_pages)' href='#'>Last &rsaquo;&rsaquo;</a></li>";
              } ?>
            </ul>
    <?php
      }

      break;
  }
}
