<?php
require_once "../functions.php";

if (isset($_POST['action']) && !empty($_POST['action'])) {

  $action = $_POST['action'];

  switch ($action) {

    case 'updateactivity':
      $user_id = $_POST['userId'];
      $activity = $_POST['activity'];
      $lb = new Leaderboard();
      $lb->__set('user_id', $user_id);
      $lb->__set('action', $activity);

      $update = $lb->updateUserActivity();

      break;

    case 'getfulllb':
      $board = new Leaderboard();
      $leaders = $board->getFullLeaderboard();
      //var_dump($leaders);
      if (!empty($leaders)) {
        $member = new User();
        $output = '<table class="table table-bordered">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col" width="100">Rank</th>
                          <th scope="col">Name</th>
                          <th scope="col" width="150">Points</th>
                        </tr>
                      </thead>
                      <tbody>';
        $i = 1;
        foreach ($leaders as $leader) {
          $member->__set('user_id', $leader['user_id']);
          $user = $member->getUserName();
          //var_dump($user);
          $output .=  '<tr>
                          <th scope="row">' . $i . '</th>
                          <td><b>' . $user . '</td>
                          <td>' . $leader['total'] . '</td>
                        </tr>';
          $i++;
        }
        $output .=  '</tbody>
                      </table>';
        echo $output;
      } else {
        echo 'There is no leaderboard available right now.';
      }

      break;
    case 'getleaderboard':
      $board = new Leaderboard();
      $leaders = $board->getLeaderboard();
      //var_dump($leaders);
      if (!empty($leaders)) {
        $member = new User();
        $output = '<table class="table table-bordered">
                    <thead class="thead-light">
                      <tr>
                        <th scope="col" width="100">Rank</th>
                        <th scope="col">Name</th>
                        <th scope="col" width="150">Points</th>
                      </tr>
                    </thead>
                    <tbody>';
        $i = 1;
        foreach ($leaders as $leader) {
          $member->__set('user_id', $leader['user_id']);
          $user = $member->getUserName();
          //var_dump($user);
          $output .=  '<tr>
                        <th scope="row">' . $i . '</th>
                        <td><b>' . $user . '</td>
                        <td>' . $leader['total'] . '</td>
                      </tr>';
          $i++;
        }
        $output .=  '</tbody>
                    </table>';
        echo $output;
      } else {
        echo 'There is no leaderboard available right now.';
      }

      break;

      /*Get points for current user*/
    case 'getpoints':

      $total = 0;
      $userid = $_POST['user'];

      $board = new Leaderboard();

      $content = $board->getCounts($userid, 'VIEW_CONTENT');
      $content_score = $content * 50;
      $total += $content_score;

      $rank = 1;
      $rankList = $board->getRank($userid);
      if (!empty($rankList)) {
        foreach ($rankList as $user) {
          if ($userid != $user['user_id']) {
            $rank++;
          } else {
            break;
          }
        }
      }

      $output = '<table class="table table-bordered">
                  <thead class="thead-light">
                    <tr>
                      <th scope="col" colspan="2">Your Rank: ' . $rank . '</th>
                      <th scope="col" colspan="2">Total Points: ' . $total . '</th>                  
                    </tr>
                  </thead>';
      /*$output .= '<thead class="thead-light">
                    <tr>
                      <th scope="col">Activity for Points</th>
                      <th scope="col">Points</th>
                      <th scope="col">My Activity Count</th>
                      <th scope="col">My Points</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    <tr>
                      <th scope="row">Downloading/Viewing Content</th>
                      <td>50</td>
                      <td>' . $content . '</td>
                      <td>' . $content_score . '</td>
                    </tr>';*/
      $output .= '                
                  </tbody>
                </table>';

      echo $output;

      break;
    case 'updateLB':
      $userid = $_POST['userId'];
      //update leaderpoint points
      $activity = 'VIEW_CONTENT';
      $activityLocation = $_POST['activity'];

      $lb = new Leaderboard();
      $lb->__set('user_id', $userid);
      $lb->__set('action', $activity);
      $lb->__set('location', $activityLocation);
      $lb->__set('points', '50');

      $done = $lb->checkUserActivity();
      if ($done == 0) {
        $lb->updateUserActivity();
      }

      break;
  }
}
