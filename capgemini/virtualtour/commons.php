<div class="modal fade" id="leaderboard" tabindex="-1" role="dialog" aria-labelledby="leaderboardLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="leaderboardLabel">Leaderboard</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
      <div class="modal-body mb-0 p-0">
        <div id="leaderboardPoints" class="content scroll">
          <ul class="modal-tabs nav nav-tabs" role="tablist">
            <li id="lRanks" class="active">
              <a href="#">Leaderboard Ranks</a>
            </li>
            <li id="pointsSystem" class="">
              <a href="#">Point System</a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="ranks" style="display:block;">
              <div id="conf-lb">
                <div id="my-rank"></div>
                <div id="conf-ranks"></div>
              </div>
            </div>
            <div class="tab-pane" id="points" style="display:none;">
              <div id="terms-cond">
                <table class="table table-striped table-boredered mt-2">
                  <thead>
                    <tr>
                      <th>Activity</th>
                      <th width="100">Points</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>View Resource Center Content</td>
                      <td>50</td>
                    </tr>
                  </tbody>

                </table>

              </div>
            </div>
          </div>
        </div>



      </div>


    </div>
  </div>
</div>

<div class="modal fade" id="sentimeter" tabindex="-1" role="dialog" aria-labelledby="sentimeterLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="sentimeterLabel">Feedback</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
      <div class="modal-body mb-0 p-0">
        <!-- <div id="sentimeterPoints" class="content scroll">

        </div> -->
        <div class="container">
          <form class="my-2" action="#" id="feedback-form" method="post">
            <div class="form-group">
              <label>We greatly appreciate your feedback on this platform</label>
              <textarea class="input" name="comment" id="comment"></textarea>
            </div>
            <div class="form-group">
              <label>Rate the Conference: (1 - lowest, 5-highest)</label>
              <input type="radio" name="rating" value="1"> 1 &nbsp;
              <input type="radio" name="rating" value="2"> 2 &nbsp;
              <input type="radio" name="rating" value="3"> 3 &nbsp;
              <input type="radio" name="rating" value="4"> 4 &nbsp;
              <input type="radio" name="rating" value="5"> 5
            </div>
            <div class="form-group mt-3">
              <input type="hidden" name="user_id" id="user_id" value="<?php echo $_SESSION['userid']; ?>" required>
              <button type="button" class="btn btn-sm btn-primary" id="subFeedback">Submit Feedback</button>
            </div>
          </form>
        </div>



      </div>


    </div>
  </div>
</div>