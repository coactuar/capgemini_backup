<nav class="navbar bottom-nav">
  <ul class="nav mr-auto ml-auto">
    <li class="nav-item">
      <a class="nav-link" href="agenda.php" title=""><i class="fa fa-clipboard-list"></i><span class="hide-menu">Agenda</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="lobby.php" title="Go To Lobby"><i class="fa fa-home"></i><span class="hide-menu">Lobby</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#" onclick="gotoBoardroom()"><i class="fas fa-location-arrow"></i><span class="hide-menu">Boardroom</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#" onclick="gotoEngArea()"><i class="fa fa-chalkboard-teacher"></i><span class="hide-menu">Engagement Area</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#" onclick="gotoLab()"><i class="fas fa-network-wired"></i><span class="hide-menu">Innovation Lab Area</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="resourcecenter.php" title="Go To Resource Center"><i class="fa fa-box-open"></i><span class="hide-menu">Resource Center</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link show_leaderboard" href="#"><i class="fas fa-trophy"></i><span class="hide-menu">Leaderboard</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link show_sentimeter" href="#"><i class="fas fa-star"></i><span class="hide-menu">Feedback</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link logout" href="logout.php" title="Logout"><i class="fas fa-sign-out-alt"></i>Logout</a>
    </li>
  </ul>

</nav>