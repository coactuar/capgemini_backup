<section class="videotoplay" id="gotoengarea" style="display:none;">
    <video class="videoplayer" id="gotoengareavideo" preload="auto">
        <source src="walkthrough.mp4" type="video/mp4">
    </video>
    <div class="skip"><a href="#" onClick="skipEA()">SKIP</a></div>
</section>
<section class="videotoplay" id="gotoboardroom" style="display:none;">
    <video class="videoplayer" id="gotoboardroomvideo" preload="auto">
        <source src="walkthrough.mp4" type="video/mp4">
    </video>
    <div class="skip"><a href="#" onClick="skipBR()">SKIP</a></div>
</section>
<section class="videotoplay" id="gotoinnovation" style="display:none;">
    <video class="videoplayer" id="gotoinnovationvideo" preload="auto">
        <source src="walkthrough.mp4" type="video/mp4">
    </video>
    <div class="skip"><a href="#" onClick="skipLab()">SKIP</a></div>
</section>