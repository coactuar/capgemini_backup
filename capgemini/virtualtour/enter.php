<?php
require_once "logincheck.php";
$curr_room = 'entrance';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="welcome" style="display: block;">
    <div class="welcome-message d-flex p-4 h-100">
        <span class="align-self-end text-white">
            <h2>
                Hello <?= $user_name ?>,<br>
                We are so glad you could make it!
            </h2>
            <button class="btn btn-enter" id="btnEnter">Enter <i class="far fa-arrow-alt-circle-right ml-3"></i></button>
        </span>
    </div>
</div>
<section class="videotoplay" id="gotoentry" style="display:none;">
    <video class="videoplayer" id="gotoentryvideo" preload="auto">
        <source src="venue-entry.mp4" type="video/mp4">
    </video>
    <div class="skip"><a href="#" onClick="skipEntry()">SKIP</a></div>
</section>
<?php require_once "scripts-entry.php" ?>

<script>
    var entryVideo = document.getElementById("gotoentryvideo");
    entryVideo.addEventListener('ended', entryEnd, false);

    $(function() {
        $('#btnEnter').on('click', function() {
            $('.welcome').css('display', 'none');
            $('#gotoentry').css('display', 'block');
            gotoEntry();
        })

    });

    function gotoEntry() {
        entryVideo.currentTime = 0;
        entryVideo.play();
    }

    function entryEnd(e) {
        location.href = "lobby.php";
    }

    function skipEntry() {
        location.href = "lobby.php";
    }
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>