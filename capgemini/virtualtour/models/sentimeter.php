<?php

class Sentimeter
{

    private $ds;
    private $user_id;
    private $points;
    private $comment;
    private $table = 'tbl_sentimeter';
    public $limit = 50;

    private $userstable = 'tbl_users';

    function __construct()
    {
        $this->ds = new DataSource();
    }
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }


    function updateUserActivity()
    {
        $today = date("Y/m/d H:i:s");

        $query = "Insert into " . $this->table . "(user_id, points, comment, submitted_at) values(?, ?,?,?)";
        $paramType = 'ssss';
        $paramValue = array(
            $this->user_id,
            $this->points,
            $this->comment,
            $today
        );

        $activityid = $this->ds->insert($query, $paramType, $paramValue);
        if ($activityid > 0) {
            return 'done';
        }
        return false;
    }

    public function getFBCount()
    {
        $query = 'Select id from ' . $this->table . '';
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);
        return $count;
    }

    function getFeedbacks($offset)
    {

        $query = "SELECT * FROM " . $this->table . "," . $this->userstable . " where " . $this->table . ".user_id = " . $this->userstable . ".user_id order by submitted_at DESC  limit ?,?";
        $paramType = 'ss';
        $paramValue = array($offset, $this->limit);

        $feedbacks = $this->ds->select($query, $paramType, $paramValue);

        return $feedbacks;
    }
    function getAllFeedbacks()
    {

        $query = "SELECT * FROM " . $this->table . "," . $this->userstable . " where " . $this->table . ".user_id = " . $this->userstable . ".user_id order by submitted_at DESC";
        $paramType = '';
        $paramValue = array();

        $feedbacks = $this->ds->select($query, $paramType, $paramValue);

        return $feedbacks;
    }
}
