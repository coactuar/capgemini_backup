<?php
require_once "logincheck.php";
$curr_room = 'agenda';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content-agenda">

        <div id="bg">
            <img src="assets/img/agenda.jpg" id="bg-image">
            <a id="ses01" href="lobby.php"></a>
            <a id="ses02" href="boardroom.php"></a>
            <a id="ses03" href="engagement.php"></a>
            <a id="ses04" href="resourcecenter.php"></a>
            <a id="ses05" href="innovationlab.php"></a>
            <a id="ses06" href="lobby.php"></a>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "wt.php" ?>
<?php require_once "scripts.php" ?>


<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>