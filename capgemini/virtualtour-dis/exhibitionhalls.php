<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="https://origyn.s3.ap-south-1.amazonaws.com/exhibitionhall.jpg" usemap="#image-map">
            <map name="image-map">
                <area href="mumfermax.php" coords="257,3069,278,2391,307,2249,1409,2200,1494,2341,1487,2949" shape="poly">
                <area href="fenza.php" coords="1777,2569,1791,2109,1904,2088,1918,1961,2271,1954,2278,2032,2596,2060,2611,2512" shape="poly">
                <area href="vytal.php" coords="2985,2368,2992,1951,3077,1951,3084,1831,3635,1838,3628,1951,3699,1951,3699,2375" shape="poly">
                <area href="fenzawash.php" coords="4165,2368,4172,1965,4328,1944,4335,1838,4717,1810,4710,1951,4872,1979,4879,2375" shape="poly">
                <area href="dubagest.php" coords="5233,2508,5240,2063,5310,2056,5331,1893,5946,1900,5946,2077,6031,2098,6038,2579" shape="poly">
                <area href="iq.php" coords="6278,2941,6278,2326,6384,2304,6392,2170,7473,2227,7466,2403,7466,3054" shape="poly">
            </map>
            <a href="https://player.vimeo.com/video/481733317" id="exhVideo" class="viewvideo"></a>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>