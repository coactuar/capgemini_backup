<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$emailid = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if (empty($_POST['emailid'])) {
    $errors['email'] = 'Email ID is required';
  }
  $emailid = $_POST['emailid'];

  if (count($errors) == 0) {
    $user = new User();
    $user->__set('emailid', $emailid);
    $login = $user->userLogin();
    //var_dump($login);
    $reg_status = $login['status'];
    if ($reg_status == "error") {
      $errors['login'] = $login['message'];
    }
  }
}
?>

<?php require_once 'header.php';  ?>

<div class="d-flex flex-column h-100">
  <div id="login-bg" class="d-flex fill">
    <img src="assets/img/bg.png" class="img-fluid" alt="">
  </div>
  <div class="p-2">
    <img src="assets/img/logo.svg" class="img-fluid" alt="">
  </div>
  <div class="d-flex mx-auto my-auto">
    <div class="form-wrapper">
      <?php
      if (count($errors) > 0) : ?>
        <div class="alert alert-danger alert-msg">
          <ul class="list-unstyled">
            <?php foreach ($errors as $error) : ?>
              <li>
                <?php echo $error; ?>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      <?php endif; ?>

      <form action="" method="post">

        <div class="form-group">
          <input type="text" name="emailid" id="emailid" class="input" placeholder="Enter your Email ID" value="<?= $emailid ?>">
        </div>

        <div class="form-group">
          <input type="submit" name="loginuser-btn" id="btnLogin" class="form-submit btn-login" value="Login" />
        </div>
      </form>
    </div>
  </div>

</div>

<!-- <div class="container-fluid d-flex h-100 flex-column">
  <div class="p-2">
    <img src="assets/img/logo.svg" class="img-fluid" alt="">
  </div>
  <div id="login-bg" class="d-flex fill">
    <img src="assets/img/bg.png" class="img-fluid" alt="">
  </div>
  <div class="row mb-1">
    <div class="col-12 col-md-6 col-lg-4 ml-auto mr-3">
      <div class="right-area-wrapper">
        <div class="row mt-1">
          <div class="col-12">
            <div class="form-wrapper">
              <?php
              if (count($errors) > 0) : ?>
                <div class="alert alert-danger alert-msg">
                  <ul class="list-unstyled">
                    <?php foreach ($errors as $error) : ?>
                      <li>
                        <?php echo $error; ?>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif; ?>

              <form action="" method="post">

                <div class="form-group">
                  <input type="text" name="emailid" id="emailid" class="input" placeholder="Enter your Email ID" value="<?= $emailid ?>">
                </div>

                <div class="form-group">
                  <input type="submit" name="loginuser-btn" id="btnLogin" class="form-submit btn-login" value="Login" />
                </div>
              </form>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</div> -->
<?php require_once 'ga.php';  ?>
<?php require_once 'footer.php';  ?>