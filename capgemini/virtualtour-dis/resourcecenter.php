<?php
require_once "logincheck.php";
$curr_room = 'halls';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content" class="halls">
        <div id="main-area">
            <div id="hallsCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item stall1 active">
                        <img src="assets/img/stall1.jpg" class="d-block w-100" alt="...">
                        <a href="https://talent.capgemini.com/global/news/116618" data-docid="b6f373e34caf96c73c631cd554981e8536b216f02a546b6ef92e149246b426b8" class="lb resdl" target="_blank" id="tv-video"></a>
                        <a href="https://talent.capgemini.com/global/news/116557" data-docid="2b87f81b411d0d7a4bb7fb2a8c15ab239dc7bcf41c4927bcd6b4771ce5bf3a88" class="lb resdl" target="_blank" id="kickoff"></a>
                    </div>
                    <div class="carousel-item stall2">
                        <img src="assets/img/stall2.jpg" class="d-block w-100" alt="...">
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/Examiner+%E2%80%93+Genpact.pdf" class="showpdf lb resdl" data-docid="a9083ff3145fe78b9c35ae9f97a47f54d46f4be5777c6797164f179da1612d23" id="genpact"></a>
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/Examiner+%E2%80%93+TCS.pdf" class="showpdf lb resdl" data-docid="af9720c8724d99eab4964ed01c9cc1d03c0fb29c2b8fc4d8ca44444ae61302f9" id="tcs"></a>
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/Examiner+%E2%80%93+Wipro.pdf" class="showpdf lb resdl" data-docid="d54f147624c504cf2cce82b6902674819b23bdb8b23df36278293a92db77b1a0" id="wipro"></a>
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/Examiner+%E2%80%93+Infosys.pdf" class="showpdf lb resdl" data-docid="ee5ef222a42979b524a8d1b8ed3d33f4d91157f7b8a5d7e79d97275c0e8a6377" id="infosys"></a>
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/Examiner+%E2%80%93+WNS.pdf" class="showpdf lb resdl" data-docid="f8fe1e9360ddc28c5ca604c5e51d88a109f31bf06e44911b2173bc420ebd186d" id="wns"></a>
                    </div>
                    <div class="carousel-item stall3">
                        <img src="assets/img/stall3.jpg" class="d-block w-100" alt="...">
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/RFPIO+Benchmark+Report.pdf" class="showpdf lb resdl" data-docid="9d7c84935d3feb62eb0d030b8f442a03eebc729d33b1de3d5100a43031d8bcad" id="report"></a>
                        <a href="https://capgemini.sharepoint.com/sites/BSvPursuitCenter/SitePages/RFPIO.aspx" class="lb resdl" target="_blank" data-docid="5a8bb826afb6b7ac353447003045a43854c1a1c14ea65e14e17ecab83e2ddd39" id="center"></a>
                    </div>
                    <div class="carousel-item stall4">
                        <img src="assets/img/stall4.jpg" class="d-block w-100" alt="...">
                        <a href="https://player.vimeo.com/video/511905996" class="vidlb viewvideo vidview" data-vidid="11e920b9d36efd9f5e5640170aa0b9cdc20547cb836cb7af90e434d4ee7fcd21" id="video"></a>
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/Innovation+Nation.pdf" class="showpdf lb resdl" data-docid="2716818c9330e1dbb570048cd1ca5939d311a79f03bb58e8b1bc4e02919a944f" id="innovation"></a>
                    </div>
                    <div class="carousel-item stall5">
                        <img src="assets/img/stall5.jpg" class="d-block w-100" alt="...">
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/Digital+Mastery.pdf" class="showpdf lb resdl" data-docid="0518d10b2b7f4271745f35c78077b300bbeb1c892dd28b711e3b68e4f37aa079" id="mastery"></a>
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/The+Future+of+Work.pdf" class="showpdf lb resdl" data-docid="62a900c0fbf70b22609c369eefc4e8489f4d0112f9c24c752a1041ebe08c2cb3" id="future"></a>
                        <a href="https://cgvirtualtour2021.s3.ap-south-1.amazonaws.com/Fast+Forward.pdf" class="showpdf lb resdl" data-docid="c73b95ddb6339b85c84d2dfe2ceedfe609cbca5c20852fe3fe9ba2e4c5957059" id="forward"></a>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#hallsCarousel" role="button" data-slide="prev">
                    <!-- <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span> -->
                    Prev
                </a>
                <a class="carousel-control-next" href="#hallsCarousel" role="button" data-slide="next">
                    <!-- <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span> -->
                    Next
                </a>
            </div>
            <div class="menu-list">
                <nav class="navbar navbar-expand-md">
                    <div class="collapse navbar-collapse" id="bottomNav">
                        <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-target="#hallsCarousel" data-slide-to="0">Capgemini Group 2021</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-target="#hallsCarousel" data-slide-to="1">Competitor Study</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-target="#hallsCarousel" data-slide-to="2">Proposal Management – RFPIO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-target="#hallsCarousel" data-slide-to="3">Frictionless Enterprise</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-target="#hallsCarousel" data-slide-to="4">Capgemini Research Institute</a>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
        </div>

        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<?php require_once "exhib-script.php" ?>
<script>
    $(function() {
        $('#hallsCarousel').carousel({
            interval: false,
            keyboard: true,

        });
    });
</script>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>