<nav class="navbar top-nav">
  <ul class="nav mr-auto">
    <li class="nav-item">
      <img src="assets/img/logo.svg" class="img-fluid logo" alt="Capgemini">
    </li>
  </ul>
  <ul class="nav ml-auto">
    <li class="nav-item border-right">
      <ul id="attendance">
        <li>
          <div id="visitorCount"><b>Visitors:</b> 1 </div>
        </li>
        <li>
          <div id="onlineCount"><b>Online:</b> 1</div>
        </li>
        <li>
          <div id="onpageCount"><b>On this page:</b> 1</div>
        </li>
      </ul>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="#"> Hello, <?= $user_name ?>!</a>
    </li>

  </ul>
</nav>