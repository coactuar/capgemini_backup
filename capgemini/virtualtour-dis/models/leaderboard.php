<?php

class Leaderboard
{

    private $ds;
    private $user_id;
    private $action;
    private $points;
    private $location;
    private $table = 'tbl_useractivity';
    public $limit = 50;

    private $userstable = 'tbl_users';
    private $exhibvistable = 'tbl_exhibitorvisitors';
    private $sessionatttable = 'tbl_sessionattendees';


    function __construct()
    {
        $this->ds = new DataSource();
    }
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    function checkUserActivity()
    {
        $query = "SELECT * from " . $this->table . " where user_id = ? and action=? and location=?";
        $paramType = 'sss';
        $paramValue = array(
            $this->user_id,
            $this->action,
            $this->location
        );

        $status = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $status;
    }
    function updateUserActivity()
    {
        //$this->points = $this->getPoints($this->action);
        $query = "Insert into " . $this->table . "(user_id, action, points, location) values(?, ?, ?, ?)";
        $paramType = 'ssis';
        $paramValue = array(
            $this->user_id,
            $this->action,
            $this->points,
            $this->location
        );

        $activityid = $this->ds->insert($query, $paramType, $paramValue);
        return $activityid;
    }

    function getFullLeaderboard()
    {
        $query = "SELECT " . $this->table . ".user_id,sum(points) as total FROM " . $this->table . ", " . $this->userstable . " where " . $this->table . ".user_id = " . $this->userstable . ".user_id GROUP by user_id order by total DESC";
        $paramType = '';
        $paramValue = array();

        $leaderboard = $this->ds->select($query, $paramType, $paramValue);

        return $leaderboard;
    }

    function getLeaderboard()
    {
        $query = "SELECT " . $this->table . ".user_id,sum(points) as total FROM " . $this->table . ", " . $this->userstable . " where " . $this->table . ".user_id = " . $this->userstable . ".user_id GROUP by user_id order by total DESC limit 50";
        $paramType = '';
        $paramValue = array();

        $leaderboard = $this->ds->select($query, $paramType, $paramValue);

        return $leaderboard;
    }

    function getCounts($userid, $action)
    {
        $query = "select * from " . $this->table . " where user_id =? and action = ?";
        $paramType = 'ss';
        $paramValue = array(
            $userid,
            $action
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    function getExhibCounts($userid)
    {
        $min_time = 15 * 60; //15min
        //check time spent in each stall
        $query = "SELECT exhib_id, SUM(TIMESTAMPDIFF(SECOND,entry_time, exit_time)) as total FROM " . $this->exhibvistable . " WHERE user_id=? GROUP by exhib_id HAVING total > ?";
        $paramType = 'ss';
        $paramValue = array(
            $userid,
            $min_time
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }
    function getSessCounts($userid)
    {
        $min_time = 2 * 60 * 60; //2hrs
        //check time spent in each session
        $query = "SELECT sessionid, SUM(TIMESTAMPDIFF(SECOND,join_time, leave_time)) as total FROM " . $this->sessionatttable . " WHERE userid=? GROUP by sessionid HAVING total > ?";
        $paramType = 'ss';
        $paramValue = array(
            $userid,
            $min_time
        );

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    function getLBCounts()
    {
        $query = "select distinct user_id from " . $this->table;
        $paramType = '';
        $paramValue = array();

        $count = $this->ds->getRecordCount($query, $paramType, $paramValue);

        return $count;
    }

    function getRank($userid)
    {

        $query = "SELECT user_id, sum(points) as total FROM " . $this->table . " GROUP by user_id order by total DESC";
        $paramType = '';
        $paramValue = array();

        $ranks = $this->ds->select($query, $paramType, $paramValue);

        return $ranks;
    }
}
