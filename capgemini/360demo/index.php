<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Capgemini - Demo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.css"/>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/pannellum@2.5.6/build/pannellum.js"></script>
    <link rel="stylesheet" href="magnific-popup.css">
    <script src="jquery.min.js"></script>
    <script src="jquery.magnific-popup.min.js"></script>
    <style>
    html, body{
        height:100%;
    }
    body{
        margin:0;
        padding:0;
    }
    #panorama {
        width: 100%;
        height: 100vh;
    }
    #controls {
        position: absolute;
        bottom: 0;
        z-index: 2;
        text-align: center;
        width: 100%;
        padding-bottom: 3px;
    }
    .ctrl {
        padding: 8px 5px;
        width: 30px;
        text-align: center;
        background: rgba(200, 200, 200, 0.8);
        display: inline-block;
        cursor: pointer;
    }
    .ctrl:hover {
        background: rgba(200, 200, 200, 1);
    }
    .image-hotspot {
        height: 225px;
        width: 300px;
        background-image:url(sample.jpg);
        background-position:center;
        background-size:cover;
    }
    .custom-hotspot {
        height: 30px;
        width: 30px;
        /*background: #f00;*/
    }
    .custom-hotspot:before {
      content: "";
      position: relative;
      display: block;
      width: 300%;
      height: 300%;
      box-sizing: border-box;
      margin-left: -100%;
      margin-top: -100%;
      border-radius: 45px;
      background-color: #01a4e9;
      -webkit-animation: pulse-ring 1.25s cubic-bezier(0.215, 0.61, 0.355, 1) infinite;
              animation: pulse-ring 1.25s cubic-bezier(0.215, 0.61, 0.355, 1) infinite;
    }
    .custom-hotspot:after {
      content: "";
      position: absolute;
      left: 0;
      top: 0;
      display: block;
      width: 100%;
      height: 100%;
      background-color: white;
      border-radius: 15px;
      box-shadow: 0 0 8px rgba(0, 0, 0, 0.3);
      -webkit-animation: pulse-dot 1.25s cubic-bezier(0.455, 0.03, 0.515, 0.955) -0.4s infinite;
              animation: pulse-dot 1.25s cubic-bezier(0.455, 0.03, 0.515, 0.955) -0.4s infinite;
    }
    
    @-webkit-keyframes pulse-ring {
  0% {
    transform: scale(0.33);
  }
  80%, 100% {
    opacity: 0;
  }
}

@keyframes pulse-ring {
  0% {
    transform: scale(0.33);
  }
  80%, 100% {
    opacity: 0;
  }
}
@-webkit-keyframes pulse-dot {
  0% {
    transform: scale(0.8);
  }
  50% {
    transform: scale(1);
  }
  100% {
    transform: scale(0.8);
  }
}
@keyframes pulse-dot {
  0% {
    transform: scale(0.8);
  }
  50% {
    transform: scale(1);
  }
  100% {
    transform: scale(0.8);
  }
}

    div.custom-tooltip span {
        visibility: hidden;
        position: absolute;
        border-radius: 3px;
        background-color: #fff;
        color: #000;
        text-align: center;
        max-width: 200px;
        padding: 5px 10px;
        margin-left: -220px;
        cursor: default;
    }
    div.custom-tooltip:hover span{
        visibility: visible;
    }
    div.custom-tooltip:hover span:after {
        content: '';
        position: absolute;
        width: 0;
        height: 0;
        border-width: 10px;
        border-style: solid;
        border-color: #fff transparent transparent transparent;
        bottom: -20px;
        left: -10px;
        margin: 0 50%;
    }
    .mfp-iframe-holder .mfp-content{
        max-width: 1200px !important;
    }
    </style>
</head>
<body>

<div id="panorama">
    <div id="controls">
        <div class="ctrl" id="pan-up">&#9650;</div>
        <div class="ctrl" id="pan-down">&#9660;</div>
        <div class="ctrl" id="pan-left">&#9664;</div>
        <div class="ctrl" id="pan-right">&#9654;</div>
        <div class="ctrl" id="zoom-in">&plus;</div>
        <div class="ctrl" id="zoom-out">&minus;</div>
    </div>
</div>
<script>
viewer = pannellum.viewer('panorama', {
    "type": "equirectangular",
    "panorama": "pano01.jpg",
    "autoLoad": true,
    "title": "Capgemini Office",
    "pitch": 0,
    "yaw": 0,
    "hfov": 120,
    "showControls": false,
    //"hotSpotDebug": true,
    "hotSpots": [
        {
            "pitch":0.4,
            "yaw": 43,
            "cssClass": "image-hotspot",            
        },
        {
            "pitch": 14.1,
            "yaw": 1.5,
            "cssClass": "custom-hotspot",
            "clickHandlerFunc": hs01handler,
            "clickHandlerArgs": ""
        },
        {
            "pitch": 14.1,
            "yaw": -20.5,
            "cssClass": "custom-hotspot",
            "clickHandlerFunc": hs02handler,
            "clickHandlerArgs": ""
        },
        {
            "pitch": -5.3,
            "yaw": 123.4,
            "cssClass": "custom-hotspot",
            "createTooltipFunc": hotspot,
            "createTooltipArgs": "Go to Director's Office"
        },
        {
            "pitch": -2,
            "yaw": 93.6,
            "cssClass": "custom-hotspot",
            "clickHandlerFunc": hs03handler,
            "clickHandlerArgs": ""
        }
    ]
});
// Make buttons work
document.getElementById('pan-up').addEventListener('click', function(e) {
    viewer.setPitch(viewer.getPitch() + 10);
});
document.getElementById('pan-down').addEventListener('click', function(e) {
    viewer.setPitch(viewer.getPitch() - 10);
});
document.getElementById('pan-left').addEventListener('click', function(e) {
    viewer.setYaw(viewer.getYaw() - 10);
});
document.getElementById('pan-right').addEventListener('click', function(e) {
    viewer.setYaw(viewer.getYaw() + 10);
});
document.getElementById('zoom-in').addEventListener('click', function(e) {
    viewer.setHfov(viewer.getHfov() - 10);
});
document.getElementById('zoom-out').addEventListener('click', function(e) {
    viewer.setHfov(viewer.getHfov() + 10);
});

// Hot spot creation function
function hotspot(hotSpotDiv, args) {
    hotSpotDiv.classList.add('custom-tooltip');
    var span = document.createElement('span');
    span.innerHTML = args;
    hotSpotDiv.appendChild(span);
    span.style.width = span.scrollWidth - 20 + 'px';
    span.style.marginLeft = -(span.scrollWidth - hotSpotDiv.offsetWidth) / 2 + 'px';
    span.style.marginTop = -span.scrollHeight - 12 + 'px';
}
function hs01handler(){
    //alert();
    $.magnificPopup.open({
      items: {
        src: 'sample.pdf'
      },
      type: 'iframe'
    
      
    }, 0);
}
function hs02handler(){
    //alert();
    $.magnificPopup.open({
      items: {
        src: 'sample.html'
      },
      type: 'iframe'
    
      
    }, 0);
}
function hs03handler(){
    alert('Way to Enter Conference Room');
}
</script>

</body>
</html>