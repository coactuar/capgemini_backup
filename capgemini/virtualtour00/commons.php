<div class="modal fade" id="leaderboard" tabindex="-1" role="dialog" aria-labelledby="leaderboardLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="leaderboardLabel">Leaderboard</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">&times;</span>
        </button>
      </div>
      <div class="modal-body mb-0 p-0">
        <div id="leaderboardPoints" class="content scroll">
          <ul class="modal-tabs nav nav-tabs" role="tablist">
            <li id="lRanks" class="active">
              <a href="#">Leaderboard Ranks</a>
            </li>
            <li id="pointsSystem" class="">
              <a href="#">Point System</a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="ranks" style="display:block;">
              <div id="conf-lb">
                <div id="my-rank"></div>
                <div id="conf-ranks"></div>
              </div>
            </div>
            <div class="tab-pane" id="points" style="display:none;">
              <div id="terms-cond">
                <table class="table table-striped table-boredered mt-2">
                  <thead>
                    <tr>
                      <th>Activity</th>
                      <th width="100">Points</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>View Resource Center Content</td>
                      <td>50</td>
                    </tr>
                  </tbody>

                </table>

              </div>
            </div>
          </div>
        </div>



      </div>


    </div>
  </div>
</div>