<?php
require_once "logincheck.php";
$curr_room = 'westwing';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="pano-view">
            <div id="controls">
                <div class="ctrl" id="pan-up">&#9650;</div>
                <div class="ctrl" id="pan-down">&#9660;</div>
                <div class="ctrl" id="pan-left">&#9664;</div>
                <div class="ctrl" id="pan-right">&#9654;</div>
                <div class="ctrl" id="zoom-in">&plus;</div>
                <div class="ctrl" id="zoom-out">&minus;</div>
            </div>
        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script>
    viewer = pannellum.viewer('pano-view', {
        "type": "equirectangular",
        "panorama": "assets/img/breakout_room.jpg",
        "autoLoad": true,
        //"title": "Breakout Room",
        "pitch": 0.5,
        "yaw": 20.9,
        "hfov": 100,
        "showControls": false,
        "autoRotate": -2,
        "autoRotateInactivityDelay": 5000,
        //"hotSpotDebug": true,
        "hotSpots": [{
                "pitch": -3.3,
                "yaw": -10,
                "cssClass": "hotspot",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Honesty and Trust",
                "type": "info",
                "URL": "https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDU2NjMyOTUtZTk3My00MTAxLThjYWMtNGRmNTVlOTljNmJm%40thread.v2/0?context=%7b%22Tid%22%3a%2276a2ae5a-9f00-4f6b-95ed-5d33d77c4d61%22%2c%22Oid%22%3a%22023428c9-b1a4-43f8-9c28-939b852f720b%22%7d"
            },
            {
                "pitch": -2.8,
                "yaw": 4.5,
                "cssClass": "hotspot",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Freedom and Team Spirit",
                "type": "info",
                "URL": "https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzdmMGFjZWItNjUyMS00MTk1LTg2YWQtZmNjZTVmZmE5OTI1%40thread.v2/0?context=%7b%22Tid%22%3a%2276a2ae5a-9f00-4f6b-95ed-5d33d77c4d61%22%2c%22Oid%22%3a%22023428c9-b1a4-43f8-9c28-939b852f720b%22%7d"
            },
            {
                "pitch": -2.5,
                "yaw": 36.7,
                "cssClass": "hotspot",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Boldness and Modesty",
                "type": "info",
                "URL": "https://teams.microsoft.com/l/meetup-join/19%3ameeting_MmY5OGE4ZWItNWQ0MS00N2NlLWI0OTItYjI4MDU2ZjBjZmYz%40thread.v2/0?context=%7b%22Tid%22%3a%2276a2ae5a-9f00-4f6b-95ed-5d33d77c4d61%22%2c%22Oid%22%3a%22023428c9-b1a4-43f8-9c28-939b852f720b%22%7d"
            },
            {
                "pitch": -3.7,
                "yaw": 113.8,
                "cssClass": "hotspot",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Fun",
                "type": "info",
                "URL": "https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDQ3N2E1OTgtODNjNS00YTQ5LWE1NDAtOGQxZjBhOTllMGEz%40thread.v2/0?context=%7b%22Tid%22%3a%2276a2ae5a-9f00-4f6b-95ed-5d33d77c4d61%22%2c%22Oid%22%3a%22023428c9-b1a4-43f8-9c28-939b852f720b%22%7d"
            },
            {
                "pitch": -1.8,
                "yaw": -102.3,
                "cssClass": "hotspot",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Capability and Visibility",
                "type": "info",
                "URL": "https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZjVhYzZmMTctNDNkZC00MjE5LTk0MWQtZDYzNzMyN2FlNTdi%40thread.v2/0?context=%7b%22Tid%22%3a%2276a2ae5a-9f00-4f6b-95ed-5d33d77c4d61%22%2c%22Oid%22%3a%22023428c9-b1a4-43f8-9c28-939b852f720b%22%7d"
            },
            /* {
                "pitch": -1.6,
                "yaw": -86.2,
                "cssClass": "hotspot",
                "createTooltipFunc": hotspot,
                "createTooltipArgs": "Capability and Visibility"
            } */
        ]
    });
    // Make buttons work
    document.getElementById('pan-up').addEventListener('click', function(e) {
        viewer.setPitch(viewer.getPitch() + 10);
    });
    document.getElementById('pan-down').addEventListener('click', function(e) {
        viewer.setPitch(viewer.getPitch() - 10);
    });
    document.getElementById('pan-left').addEventListener('click', function(e) {
        viewer.setYaw(viewer.getYaw() - 10);
    });
    document.getElementById('pan-right').addEventListener('click', function(e) {
        viewer.setYaw(viewer.getYaw() + 10);
    });
    document.getElementById('zoom-in').addEventListener('click', function(e) {
        viewer.setHfov(viewer.getHfov() - 10);
    });
    document.getElementById('zoom-out').addEventListener('click', function(e) {
        viewer.setHfov(viewer.getHfov() + 10);
    });



    function hs01handler() {
        //alert();
        $.magnificPopup.open({
            items: {
                src: 'sample.pdf'
            },
            type: 'iframe'


        }, 0);
    }

    function hs02handler() {
        //alert();
        $.magnificPopup.open({
            items: {
                src: 'sample.html'
            },
            type: 'iframe'


        }, 0);
    }

    function hs03handler() {
        alert('Way to Enter Conference Room');
    }
</script>

<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>