<?php
require_once 'functions.php';

$user_id = $_GET['u'];
$user = new User();
$user->__set('user_id', $user_id);
$chkUser = $user->getUser();
//var_dump($chkUser);
if (empty($chkUser)) {
  header('location: ./');
}

$errors = [];
$succ = '';

$emailid = $chkUser[0]['emailid'];
$password = '';
$cfrmpassword = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if (empty($_POST['password'])) {
    $errors['password'] = 'Password is required';
  }
  if (empty($_POST['cnfrmpassword'])) {
    $errors['cnfrmpassword'] = 'Password confirmation is required';
  }
  $emailid = $_POST['emailid'];
  $password = $_POST['password'];
  $cnfrmpassword = $_POST['cnfrmpassword'];

  if ($password == 'capgemini') {
    $errors['oldpassword'] = "Password can not be 'capgemini' ";
  }

  if ($cnfrmpassword != $password) {
    $errors['samepassword'] = 'Password do not match';
  }

  if (count($errors) == 0) {
    $user = new User();
    $user->__set('emailid', $emailid);
    $user->__set('password', $password);
    $login = $user->updatePwd();
    //var_dump($login);
    $reg_status = $login['status'];
    if ($reg_status == "error") {
      $errors['login'] = $login['message'];
    } else {
      $succ = $login['message'];
    }
  }
}
?>

<?php require_once 'header.php';  ?>
<div class="d-flex flex-column h-100">
  <div id="login-bg" class="d-flex fill">
    <img src="assets/img/bg.jpg" width="100%" alt="">
    <div class="email-area d-flex">
      <a href="mailto:cvebsv.in@capgemini.com"><img src="assets/img/email-icon.png" class="mx-auto my-auto" alt=""></a>
    </div>
    <div class="login-area show">
      <div class="form-wrapper">
        <?php
        if (count($errors) > 0) : ?>
          <div class="alert alert-danger alert-msg">
            <ul class="list-unstyled">
              <?php foreach ($errors as $error) : ?>
                <li>
                  <?php echo $error; ?>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif;
        if ($succ != '') : ?>
          <div class="alert alert-success alert-msg">
            <?php echo $succ; ?>
            <br>
            <a href='./'>Login with new password</a>
          </div>
        <?php endif; ?>
        <h5>Change Password</h5>
        <form action="" method="post">

          <div class="form-group">
            <input type="hidden" name="emailid" id="emailid" class="input" value="<?= $emailid ?>" readonly>
          </div>
          <div class="form-group">
            <input type="password" name="password" id="password" class="input" placeholder="Enter new Password">
          </div>
          <div class="form-group">
            <input type="password" name="cnfrmpassword" id="cnfrmpassword" class="input" placeholder="Enter new Password once more">
          </div>

          <div class="form-group">
            <input type="submit" name="changepwd-btn" id="btnChangePwd" class="form-submit btn-login" value="Update Password" />
          </div>
        </form>
      </div>
      <div class="slide-area d-flex">
        <a href="#" class="action mx-auto my-auto rotate">
          <i class=" fas fa-angle-double-right"></i>
        </a>
      </div>
    </div>

  </div>


</div>
<script src="//code.jquery.com/jquery-latest.js"></script>
<script>
  $(function() {
    $('.action').on('click', function() {
      $('.login-area').toggleClass('show');
      $('.action').toggleClass('rotate');

    });

  })
</script>
<?php require_once 'ga.php';  ?>
<?php require_once 'footer.php';  ?>