<?php
require_once 'functions.php';

$errors = [];
$succ = '';

$emailid = '';
$password = '';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if (empty($_POST['emailid'])) {
    $errors['email'] = 'Email ID is required';
  }
  if (empty($_POST['password'])) {
    $errors['password'] = 'Password is required';
  }
  $emailid = $_POST['emailid'];
  $password = $_POST['password'];

  if (count($errors) == 0) {
    $user = new User();
    $user->__set('emailid', $emailid);
    $user->__set('password', $password);
    $login = $user->userLogin();
    //var_dump($login);
    $reg_status = $login['status'];
    if ($reg_status == "error") {
      $errors['login'] = $login['message'];
    }
  }
}
?>

<?php require_once 'header.php';  ?>

<div class="d-flex flex-column h-100">
  <div id="login-bg" class="d-flex fill">
    <img src="assets/img/bg.jpg" width="100%" alt="">
    <div class="email-area d-flex">
      <a href="mailto:cvebsv.in@capgemini.com"><img src="assets/img/email-icon.png" class="mx-auto my-auto" alt=""></a>
    </div>
    <div class="login-area <?= ($_SERVER['REQUEST_METHOD'] == 'POST') ? 'show' : '' ?>">
      <div class="form-wrapper">
        <?php
        if (count($errors) > 0) : ?>
          <div class="alert alert-danger alert-msg">
            <ul class="list-unstyled">
              <?php foreach ($errors as $error) : ?>
                <li>
                  <?php echo $error; ?>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>
        <h5>Login</h5>
        <form action="" method="post">

          <div class="form-group">
            <input type="email" name="emailid" id="emailid" class="input" placeholder="Enter your Email ID" value="<?= $emailid ?>">
          </div>
          <div class="form-group">
            <input type="password" name="password" id="password" class="input" placeholder="Enter your Password">
          </div>

          <div class="form-group">
            <input type="submit" name="loginuser-btn" id="btnLogin" class="form-submit btn-login" value="Login" />
          </div>
        </form>
      </div>
      <div class="slide-area d-flex">
        <a href="#" class="action mx-auto my-auto <?= ($_SERVER['REQUEST_METHOD'] == 'POST') ? 'rotate' : '' ?>">
          <i class="fas fa-angle-double-right"></i>
        </a>
      </div>
    </div>

  </div>


</div>
<script src="//code.jquery.com/jquery-latest.js"></script>
<script>
  $(function() {
    $('.action').on('click', function() {
      $('.login-area').toggleClass('show');
      $('.action').toggleClass('rotate');

    });

  })
</script>
<?php require_once 'ga.php';  ?>
<?php require_once 'footer.php';  ?>