<?php
require_once '../functions.php';
require_once 'logincheck.php';
?>
<?php
require_once 'header.php';
require_once 'nav.php';
?>
<div class="container-fluid">
    <div id="superdashboard">
        <div class="row">
            <div class="col-12 col-md-6">
                <h6>Resources</h6>
                <?php

                $exhib = new Exhibitor();
                $resList = $exhib->getResourcesRpt();
                //var_dump($resList);

                if (!empty($resList)) {
                ?>
                    <table class="table table-borderless table-striped">
                        <?php
                        foreach ($resList as $res) {
                        ?>
                            <tr>
                                <td><?= '<b>' . $res['exhib_name'] . '</b> - ' . $res['resource_title']; ?></td>
                                <td><?= $res['download_count']; ?></td>
                                <td width="50"><a href="exhresdl.php?e=<?= $res['resource_id']; ?>"><i class="fas fa-download"></i></a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>

                <?php
                }

                ?>
                <h6>Videos</h6>
                <?php
                $exhib = new Exhibitor();
                $vidList = $exhib->getVideosRpt();
                //var_dump($vidList);
                if (!empty($vidList)) {
                ?>
                    <table class="table table-borderless table-striped">
                        <?php
                        foreach ($vidList as $vid) {
                        ?>
                            <tr>
                                <td><?= '<b>' . $vid['exhib_name'] . '</b> - ' . $vid['video_title']; ?></td>
                                <td><?= $vid['views']; ?></td>
                                <td width="50"><a href="exhvidview.php?e=<?php echo $vid['video_id']; ?>"><i class="fas fa-download"></i></a></td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>

                <?php
                }

                ?>
            </div>


        </div>
    </div>
</div>
<?php
require_once 'scripts.php';
?>
<?php
require_once 'footer.php';
?>